# -*- coding: utf-8 -*-
from flask import Flask, request
from models.product import Product
from models.venta import Venta

app = Flask(__name__)


@app.route("/")
def index():
    return "Welcome to Technology World"


@app.route("/techworld/search/<int:product_id>")
def search_product(product_id):
    product_model = Product({})
    product_data = product_model.browse(product_id)
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/techworld/list_product")
def list_product():
    product_model = Product({})
    product_data = product_model.list_product()
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/techworld/searchbymeasure/<int:measure_id>")
def search_productbymeasure(measure_id):
    product_model = Product({})
    product_data = product_model.browsebymeasure(measure_id)
    return str(product_data)


@app.route("/techworld/create", methods=['POST'])
def create_product():
    product_model = Product(request.json)
    product_model.create_product()

    return str('Se creo de forma exitosa el producto')


@app.route("/techworld/list_venta")
def list_venta():
    venta_model = Venta({})
    venta_data = venta_model.list_venta()
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(venta_data)

@app.route("/techworld/searchbyventa/<int:estado>")
def search_productbyventa(estado):
    venta_model = Venta({})
    venta_data = venta_model.browsebyventa(estado)
    return str(venta_data)



@app.route("/techworld/createventa", methods=['POST'])
def create_venta():
    venta_model = Venta(request.json)
    venta_model.create_venta()

    return str('Se creo de forma exitosa el producto')


@app.route("/techworld/searchventa/<int:venta_id>")
def search_venta(venta_id):
    venta_model = Venta({})
    venta_data = venta_model.browse(venta_id)
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(venta_data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1366)
